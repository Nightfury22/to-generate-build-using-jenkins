package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreatePage extends ProjectMethods{

	public CreatePage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.LINK_TEXT, using="Create Lead") WebElement clickLeadbutton;
	public CreatePage clickLead() 
	{
		//WebElement clickLeadbutton = locateElement("LinkText", "Create Lead");
		click(clickLeadbutton);
		return this;
	}
	@FindBy(how = How.ID, using="createLeadForm_companyName") WebElement companyName;
	public CreatePage clickCompany(String compName) 
	{
		clearAndType(companyName, compName);
		return this;
	}
	@FindBy(how = How.ID, using="createLeadForm_firstName") WebElement firstName;
	public CreatePage clickFirstname(String fName) 
	{
		clearAndType(firstName, fName);
		return this;
	}
	@FindBy(how = How.ID, using="createLeadForm_lastName") WebElement lastName;
	public CreatePage clickLastname(String lName) 
	{
		clearAndType(lastName, lName);
		return this;
	}
	@FindBy(how = How.XPATH, using="//input[@class='smallSubmit']") WebElement clickSubmit;
	public ViewLead clickSubmit() 
	{
		click(clickSubmit);
		return new ViewLead();
	}
	
	
}