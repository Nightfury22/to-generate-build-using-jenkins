package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLead extends ProjectMethods{

	public ViewLead() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.XPATH, using="//span[@id='viewLead_firstName_sp']") WebElement text;
	public ViewLead getTextLeadpage() throws InterruptedException 
	{
		Thread.sleep(2000);//WebElement clickLeadbutton = locateElement("LinkText", "Create Lead");
		getElementText(text);
		System.out.println(text);
		return this;
	}
		
}