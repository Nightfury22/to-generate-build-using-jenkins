package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LeadPage extends ProjectMethods{

	public LeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	@FindBy(how = How.XPATH, using="//a[text()='Leads']") WebElement clickLeadbutton;
	public LeadPage clickLead() 
	{
		//WebElement clickCRM = locateElement("xpath", "//a[text()='Leads']");
		click(clickLeadbutton);
		return new LeadPage();
	}
	@FindBy(how = How.LINK_TEXT, using="Create Lead") WebElement clickCreatebutton;
	public CreatePage clickCreate() 
	{
		//WebElement clickLeadbutton = locateElement("LinkText", "Create Lead");
		click(clickCreatebutton);
		return new CreatePage();
	}
	
}