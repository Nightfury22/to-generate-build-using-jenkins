package com.framework.testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.CreatePage;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_LoginLogout";
		testDescription = "Login into leaftaps";
		testNodes="Leads";
		author = "Gayatri";
		category = "Smoke";
		dataSheetName="TC001";
	} 
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password, String compName, String fName, String lName) 
	{
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickCreate()
		.clickCompany(compName)
		.clickFirstname(fName)
		.clickLastname(lName)
		.clickSubmit();
		//.getTextLeadpage();
	}
	
}
